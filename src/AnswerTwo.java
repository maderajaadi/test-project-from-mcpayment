import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AnswerTwo {
    Scanner sc = new Scanner(System.in);

    public AnswerTwo(){
        int x = sc.nextInt(); sc.nextLine();
        int indexSize = sc.nextInt(); sc.nextLine();

        List<Integer> nums = new ArrayList<>();
        List<Integer> numsThatNotEqualsToX = new ArrayList<>();

        for(int i = 0; i < indexSize; i++){
            int number = sc.nextInt(); sc.nextLine();
            nums.add(number);
        }

        for(int j = 0; j < nums.size(); j++){
            if(nums.get(j) / 1 != x)
                numsThatNotEqualsToX.add(nums.get(j));
        }

        for(int k = 0; k < numsThatNotEqualsToX.size(); k++){
            System.out.println(numsThatNotEqualsToX.get(k));
        }

    }
}
