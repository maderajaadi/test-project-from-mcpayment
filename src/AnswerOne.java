import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AnswerOne {

    Scanner sc = new Scanner(System.in);

    public AnswerOne(){
        int arraySize = sc.nextInt(); sc.nextLine();

        List<Integer> nums = new ArrayList<>();
        List<Integer> numsThatSubtractionIsNotLessThanZero = new ArrayList<>();

        for(int i = 0; i < arraySize; i++){
            int number = sc.nextInt(); sc.nextLine();
            nums.add(number);
        }

        for(int j = 0; j < nums.size(); j++){
            int flag = 0;

            for(int k = 0; k < nums.size() ; k++){
                if(j == k)
                    continue;

                if(nums.get(j) - nums.get(k) < 0)
                    flag++;
            }
            if(flag == 0)
                numsThatSubtractionIsNotLessThanZero.add(nums.get(j));
        }

        for(int m = 0; m < numsThatSubtractionIsNotLessThanZero.size(); m++){
            System.out.println(numsThatSubtractionIsNotLessThanZero.get(m));
        }
    }

}
